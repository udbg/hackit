
use hackit::*;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name = "hkit-ij", author = "metaworm", about = "inject a dynamic library to another process")]
struct ShellArg {
    process: String,
    libpath: String,
    #[structopt(short, long, about = "the name of window hook handler")]
    window: Option<String>,
}

#[cfg(windows)]
fn main() {
    let args = ShellArg::from_args();
    let pid = if let Ok(pid) = pid_t::from_str_radix(&args.process, 10) {
        pid
    } else {
        enum_process_filter_name(&args.process).next().expect("name not found").pid()
    };
    if let Some(n) = args.window {
        let hwnd = get_window(pid).unwrap();
        inject::by_windowhook(hwnd, &args.libpath, &n).unwrap();
        return;
    } else {
        let p = Process::open(pid, None).unwrap();
        inject::by_remotethread(&p, &args.libpath).unwrap();
    }
}