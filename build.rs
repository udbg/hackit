
fn main() {
    let family = std::env::var("CARGO_CFG_TARGET_FAMILY").unwrap();
    if family == "windows" {
        let mut build = cc::Build::new();
        build.file("src/win/ldisasm.c").compile("ldisasm");
    }
}