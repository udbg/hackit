
use crate::{Address, AsByteArray};
#[cfg(windows)]
use crate::ToMbstr;
use alloc::vec::Vec;
use alloc::string::*;
use core::{
    slice::*,
    mem::{size_of, transmute, zeroed}
};
#[cfg(windows)]
use winapi::um::winnt::*;
#[cfg(windows)]
use crate::ToUnicode;

pub trait ReadMemory {
    fn read_memory<'a>(&self, addr: Address, data: &'a mut [u8]) -> Option<&'a mut [u8]>;
}

pub trait ReadMemUtil: ReadMemory {
    fn read_util<T: PartialEq + Copy>(&self, address: usize, pred: impl Fn(&T)->bool + Copy, max_count: usize) -> Vec<T> {
        const BUFLEN: usize = 100usize;
        let mut result: Vec<T> = Vec::with_capacity(BUFLEN);

        unsafe {
            let mut buf: [T; BUFLEN] = core::mem::zeroed();
            let mut addr = address;

            let pdata: *mut u8 = transmute(buf.as_mut_ptr());
            let size = buf.len() * size_of::<T>();
            let mut end = false;
            while let Some(data) = self.read_memory(addr, from_raw_parts_mut(pdata, size)) {
                let mut pos = match buf.iter().position(pred) {
                    None => buf.len(),
                    Some(pos) => { end = true; pos },
                };
                if result.len() + pos > max_count {
                    end = true;
                    pos = max_count - result.len();
                }
                result.extend_from_slice(&buf[..pos]);
                if end { break; }
                addr += data.len();
            }
        }
        return result;
    }

    #[inline(always)]
    fn read_util_eq<T: PartialOrd + Copy>(&self, address: usize, val: T, max_bytes: usize) -> Vec<T> {
        self.read_util(address, |&x| x == val, max_bytes)
    }

    #[inline(always)]
    fn read_util_lt<T: PartialOrd + Copy>(&self, address: usize, val: T, max_bytes: usize) -> Vec<T> {
        self.read_util(address, |&x| x < val, max_bytes)
    }

    fn read_cstring(&self, address: usize, max: impl Into<Option<usize>>) -> Option<Vec<u8>> {
        let result = self.read_util_eq(address, 0, max.into().unwrap_or(1000));
        if result.len() == 0 || (result.len() == 1 && result[0] < b' ') { return None; }
        Some(result)
    }

    fn read_utf8(&self, address: usize, max: impl Into<Option<usize>>) -> Option<String> {
        String::from_utf8(self.read_cstring(address, max)?).ok()
    }

    #[cfg(windows)]
    fn read_ansi(&self, address: usize, max: impl Into<Option<usize>>) -> Option<String> {
        let r = self.read_cstring(address, max)?;
        Some(r.to_unicode().to_utf8())
    }

    #[cfg(windows)]
    fn read_utf8_or_ansi(&self, address: usize, max: impl Into<Option<usize>>) -> Option<String> {
        let r = self.read_cstring(address, max)?;
        match core::str::from_utf8(&r) {
            Ok(_) => String::from_utf8(r).ok(),
            Err(_) => Some(r.to_unicode().to_utf8()),
        }
    }

    #[cfg(windows)]
    fn read_wstring(&self, address: usize, max: impl Into<Option<usize>>) -> Option<String> {
        let result = self.read_util_lt(address, b' ' as u16, max.into().unwrap_or(1000));
        if result.len() == 0 { return None; }
        Some(result.to_utf8())
    }

    fn read_array<T>(&self, addr: Address, count: usize) -> Vec<Option<T>> {
        let mut result = Vec::<Option<T>>::with_capacity(count);
        for i in 0..count {
            result.push(self.read_value::<T>(addr + size_of::<T>() * i));
        }
        result
    }

    fn read_bytes(&self, addr: Address, size: usize) -> Vec<u8> {
        let mut buf: Vec<u8> = vec![0u8; size];
        let len = match self.read_memory(addr, &mut buf) {
            Some(slice) => slice.len(), None => 0
        };
        buf.resize(len, 0); buf
    }

    #[inline]
    fn read_value<T>(&self, address: usize) -> Option<T> {
        unsafe {
            let mut val: T = zeroed();
            self.read_memory(
                address, from_raw_parts_mut(transmute::<_, *mut u8>(&mut val), size_of::<T>())
            ).and_then(|buf| if buf.len() == size_of::<T>() { Some(val) } else { None })
        }
    }

    fn read_type<T>(&self, address: Address, buf: &mut [T]) -> usize {
        unsafe {
            let size = size_of::<T>() * buf.len();
            let pdata: *mut u8 = transmute(buf.as_mut_ptr());
            let mut buf = from_raw_parts_mut(pdata, size);
            self.read_memory(address, &mut buf).map(|b| b.len() / size_of::<T>()).unwrap_or(0)
        }
    }

    fn read_level<T>(&self, address: Address, offset: &[usize]) -> Option<T> {
        let mut p = address;
        for o in offset.iter() {
            if p == 0 { return None; }
            if let Some(v) = self.read_value(p + *o) {
                p = v;
            } else { return None; }
        }
        self.read_value(p)
    }

    #[cfg(windows)]
    fn read_nt_header(&self, mod_base: usize) -> Option<(IMAGE_NT_HEADERS, usize)> {
        let dos: IMAGE_DOS_HEADER = self.read_value(mod_base)?;
        if dos.e_magic != IMAGE_DOS_SIGNATURE { return None; }
        let nt: IMAGE_NT_HEADERS = self.read_value(mod_base + dos.e_lfanew as usize)?;
        if nt.Signature != IMAGE_NT_SIGNATURE { return None; }
        Some((nt, dos.e_lfanew as usize))
    }
}

pub trait WriteMemory {
    fn write_memory(&self, address: Address, data: &[u8]) -> Option<usize>;
}

pub trait WriteMemUtil: WriteMemory {
    #[inline]
    fn write_value<T>(&self, address: usize, val: &T) -> Option<usize> {
        self.write_memory(address, val.as_byte_array())
    }

    #[inline]
    fn write_array<T>(&self, address: usize, data: &[T]) -> Option<usize> {
        self.write_memory(address, unsafe {
            from_raw_parts(data.as_ptr() as *const u8, data.len() * size_of::<T>())
        })
    }

    fn write_cstring(&self, address: usize, data: impl AsRef<[u8]>) -> Option<usize> {
        let r = data.as_ref();
        Some(self.write_memory(address, r)? + self.write_memory(address + r.len(), &[0u8])?)
    }

    #[cfg(windows)]
    fn write_wstring(&self, address: usize, data: &str) -> Option<usize> {
        self.write_array(address, data.to_unicode_with_null().as_slice())
    }
}

impl<T: ReadMemory + ?Sized> ReadMemUtil for T {}
impl<T: WriteMemory + ?Sized> WriteMemUtil for T {}